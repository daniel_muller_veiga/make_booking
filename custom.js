// change select btn state
function selectBtn (){
	var clickedIndex = $('.make_booking_select_btn').index(this);
	var selectedBtn = $('.make_booking_select_btn').eq(clickedIndex);
	var btnText = $('.make_booking_select_btn span');

	$(selectedBtn).toggleClass('selected');

	var checkBtnState = $('.make_booking_select_btn').hasClass('selected');
	var selectedBtnIcon = $('.make_booking_select_btn.selected .fa');
	var normlaBtnIcon = $('.make_booking_select_btn .fa');

	if (checkBtnState) {
		$(selectedBtnIcon).removeClass('fa-plus-circle').addClass('fa-minus-circle');
		$('.make_booking_select_btn.selected span').text('Remove');
		$(selectedBtn).removeClass('btn-primary').addClass('btn-danger');
		$('#accomodation_nbr_badge, #accomodation_continue_btn').removeClass('hidden');
	} else {
		$(normlaBtnIcon).removeClass('fa-minus-circle').addClass('fa-plus-circle');
		$('.make_booking_select_btn span').text('Select');
		$(selectedBtn).removeClass('btn-danger').addClass('btn-primary');
		$('#accomodation_nbr_badge, #accomodation_continue_btn').addClass('hidden');
	}
}

$('.make_booking_select_btn').click(selectBtn);


//active buttons
function activeStaysBtns(){
	$(this).toggleClass('active');
}

$('#accomodation_search_filters .nav-pills li').click(activeStaysBtns);

//clone accomodations results
function clone_apartments(){

	$('#searchMessage').addClass('hidden');
	$('.accomodation_apartment , #make_booking_pages').removeClass('hidden');

	$('.accomodation_apartment').clone().insertAfter('.accomodation_apartment');

}

$('#search_btn').click(clone_apartments);


// change accomodation name size
function accomodationNameSize(){

	var checkWindow = $(window).width();
	var breakPoint = 1276;


	if (checkWindow < breakPoint) {
		$('.accomodation_name').addClass('h4').removeClass('h3');
	} else{
		$('.accomodation_name').addClass('h3').removeClass('h4');
	}

}

$(document).ready(accomodationNameSize);
$(window).resize(accomodationNameSize);

//accomodation continue
$('#accomodation_continue_btn').click(function(){
	$('#client_selection_area, #accomodation_search_filters').addClass('closed');
	$('#accomodation_continue_btn').addClass('hidden');
	$('#accomodation_edit_btn').removeClass('hidden');
	setTimeout(function(){
		$('#accomodation_selection_area, #accomodation_search_filters').addClass('hidden');
	},400);
});

// client continue
$('#client_continue_btn').click(function(){
	$('#client_selection_area, #client_controls_area').addClass('closed');
	$('#client_continue_btn').addClass('hidden');
	$('#client_edit_btn').removeClass('hidden');
	setTimeout(function(){
		$('#client_selection_area, #client_controls_area').addClass('hidden');
	},400);
});

//accomodation edit
$('#accomodation_edit_btn').click(function(){
	$('#accomodation_selection_area, #accomodation_search_filters').removeClass('closed hidden');
	$('#accomodation_continue_btn').removeClass('hidden');
	$('#accomodation_edit_btn').addClass('hidden');
});

//client edit
$('#client_edit_btn').click(function(){
	$('#client_selection_area, #client_controls_area').removeClass('closed hidden');
	$('#client_continue_btn').removeClass('hidden');
	$('#client_edit_btn').addClass('hidden');
});

//add client btn
$('#client_section .nav-pills.first_level > li').click(function(){

	$(this).toggleClass('rotate');

	var haveRotate = $(this).hasClass('rotate');

	if (haveRotate) {
		$('#searchMessage_client').addClass('hidden');	
	}

	
});

$('#add_client_btn').click(function(){
	$('#new_client_forms .collapse:nth-child(1)').toggleClass('in');
});

$('#add_company_btn').click(function(){
	$('#new_client_forms .collapse:nth-child(2)').toggleClass('in');
});

//just one active client form
$('#client_section .nav-pills.second_level li').click(function(){
	$(this).each(function(){
		$('#client_section .nav-pills.second_level li').removeClass('active');
	});

	$(this).toggleClass('active');
});


//add new telehephone
$('#telephone_add').click(function(){
	$('#telephone_form').clone().insertAfter('#telephone_form');
});

//add new email
$('#email_add').click(function(){
	$('#email_form').clone().insertAfter('#email_form');
});

//add new document
$('#document_add').click(function(){
	$('#document_form').clone().insertAfter('#document_form');
});

//save attachemnt
$('#save_attachment_btn').click(function(){
	$('#save_attachment').removeClass('hidden');
});

//save client infos+next step
$('.save_client_infos').click(function(){

	$('#add_client_steps li.active i, #add_company_steps li.active i').removeClass('text-muted').addClass('text-success');
	$('#add_client_steps li.active a, #add_company_steps li.active a').removeClass('text-muted');

	var menuIndex = $('#add_client_steps li.active').index();
	var actualActive = $('#add_client_steps li').eq(menuIndex);
	var nextActive = $('#add_client_steps li').eq(menuIndex + 1);

	$(actualActive).removeClass('active');
	$(nextActive).addClass('active');
	

	var formIndex = $('#new_client_forms .collapse.in').index();
	var actualOpen = $('#new_client_forms .collapse').eq(formIndex);
	var nextOpen = $('#new_client_forms .collapse').eq(formIndex + 1);

	$(actualOpen).removeClass('in');
	$(nextOpen).addClass('in');

});

// show client card
$('#client_search_btn, #form_last_save').click(function(){
	$('#client_card').removeClass('hidden');
	$('#client_card').clone().insertAfter('#client_card');
});

$('#form_last_save').click(function(){
	$('#client_section .nav-pills.first_level > li').removeClass('rotate');
	$('#add_client_steps').removeClass('in');
	$('#add_company_steps').removeClass('in');
});


$('#selec_client_btn').click(function (){
	var clickedIndex = $('#selec_client_btn').index(this);
	var selectedBtn = $('#selec_client_btn').eq(clickedIndex);
	var btnText = $('#selec_client_btn span');

	$(selectedBtn).toggleClass('selected');

	var checkBtnState = $('#selec_client_btn').hasClass('selected');
	var selectedBtnIcon = $('#selec_client_btn.selected .fa');
	var normlaBtnIcon = $('#selec_client_btn .fa');

	if (checkBtnState) {
		$(selectedBtnIcon).removeClass('fa-plus-circle').addClass('fa-minus-circle');
		$('#selec_client_btn.selected span').text('Remove');
		$(selectedBtn).removeClass('btn-primary').addClass('btn-danger');
		$('#client_nbr_badge, #client_continue_btn').removeClass('hidden');
	} else {
		$(normlaBtnIcon).removeClass('fa-minus-circle').addClass('fa-plus-circle');
		$('#selec_client_btn span').text('Select');
		$(selectedBtn).removeClass('btn-danger').addClass('btn-primary');
		$('#client_nbr_badge, #client_continue_btn').addClass('hidden');
	}
});

//change booking type form btn
$('#booking_type_form .nav-pills li').click(function(){
	var isActive = $('#booking_type_form .nav-pills li.active').index();

	if (isActive == 1) {
		$('#make_booking_btn').removeClass('hidden');
		$('#make_pre_booking_btn').addClass('hidden');
	} else {
		$('#make_pre_booking_btn').removeClass('hidden');
		$('#make_booking_btn').addClass('hidden');
	}

});
 